import 'dart:convert';

import 'package:internet_shop/models/ProductModel.dart';
import 'package:internet_shop/api/api.dart';

class DataHelper {
  static List<ProductModel> _basketList = List<ProductModel>();

  static List<ProductModel> getProducts() {
    List<ProductModel> productList = List<ProductModel>();

    productList.add(new ProductModel(
        "Sneakers",
        "Keep going The Stretchweb and Continental Rubber outsole is designed for superior grip in wet conditions",
        "200",
        "https://www.afisha.uz/ui/materials/2019/05/0909697_b.jpeg"));
    productList.add(new ProductModel(
        "Slippers",
        "Lightweight feel. Quick-drying material",
        "100",
        "https://cs6.pikabu.ru/post_img/big/2014/10/12/10/1413128487_495056162.jpg"));

    return productList;
  }

  static getProductApi() async {
    var data = await API.getProducts();

    if (data != null) {
      List<dynamic> jsonData = json.decode(utf8.decode(data.bodyBytes));

      List<ProductModel> productList = List<ProductModel>();
      jsonData.forEach((element) {
        productList.add(ProductModel.fromJson(element));
      });

      print(productList);
      return productList;
    }

    return getProducts();
  }

  static List<ProductModel> getBasketList() {
    return _basketList;
  }

  static addTOBasket(ProductModel product) {
    _basketList.add(product);
  }

  static removeFromBasket(ProductModel product) {
    _basketList.remove(product);
  }

  static getSummaryPriceOfBasket() {
    return _basketList.fold(
        0, (summary, item) => summary + double.parse(item.price));
  }
}
