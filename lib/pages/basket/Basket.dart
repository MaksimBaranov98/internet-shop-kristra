import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internet_shop/data/dataHelper.dart';
import 'package:internet_shop/models/ProductModel.dart';
import 'package:internet_shop/pages/products/ProductDetails.dart';

class BasketScreen extends StatefulWidget {
  BasketScreen() : super();

  @override
  _BasketScreenState createState() => _BasketScreenState();
}

class _BasketScreenState extends State<BasketScreen> {
  List<ProductModel> productList = new List<ProductModel>();

  @override
  void initState() {
    productList = DataHelper.getBasketList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Basket"),
      ),
      body:
      productList.length != 0  ? (
        Column(
          children: [
            Flexible(child:
              ListView.builder(
                itemCount: productList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: ListTile(
                      leading: Container(
                        width: 36,
                        height: 36,
                        child: Image.network(productList[index].url),
                      ),
                      title: Text(productList[index].title),
                      subtitle: Text(productList[index].description),
                      trailing: IconButton(
                        icon: Icon(Icons.remove_shopping_cart),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (dialogContex) => AlertDialog(
                              title: const Text('Product successfully removed from  basket'),
                              actions: <Widget>[
                                new FlatButton(
                                  onPressed: () {
                                  Navigator.of(dialogContex).pop();
                                },
                                child: const Text('Ok'))
                              ]
                            )
                          );
                          setState(() {
                            DataHelper.removeFromBasket(productList[index]);
                          });
                        },
                      ),
                      onTap: () {
                        Navigator.of(context).push(CupertinoPageRoute(
                            builder: (context) => DetailScreen(productList[index])));
                      },
                    ),
                  );
                }
              )
            ),
            RaisedButton(
              onPressed: () {},
              child: Text("Checkout \$ " + DataHelper.getSummaryPriceOfBasket().toString())
            )
          ]
        )
      ) : (
        Center(
          child: Text(
            "Basket is empty",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal)
          ),
        )
      )
    );
  }
}
