import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  var dateOdbirth = new DateTime.utc(1998, 7, 10).toString().substring(0, 10);
  var location = "Yaroslavl";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          appBar: AppBar(title: Text("My Account")),
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                ListTile(
                  leading: CircleAvatar(
                    radius: 36,
                    child: Icon(
                      Icons.supervised_user_circle,
                      size: 72,
                    ),
                  ),
                  title: Text(
                    "Baranov Maxim",
                    style: TextStyle(fontSize: 22)
                  ),
                  subtitle: Text(
                    "maksim.baranow@yandex.ru",
                    style: TextStyle(fontSize: 18)
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            "Date of Birth: " + dateOdbirth,
                            style: TextStyle(fontSize: 22)
                          ),
                          Text(
                            "Location: " + location,
                            style: TextStyle(fontSize: 22)
                          ),
                        ],
                      )
                    )
                  ],
                )
              ],
          ),
        )
      ),
    );
  }
}
