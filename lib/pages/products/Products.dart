import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internet_shop/data/dataHelper.dart';
import 'package:internet_shop/models/ProductModel.dart';
import 'package:internet_shop/pages/products/ProductDetails.dart';

class ProductsScreen extends StatefulWidget {
  ProductsScreen() : super();

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  List<ProductModel> productList = new List<ProductModel>();

  @override
  void initState() {
    productList = DataHelper.getProducts();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Catalog"),
      ),
      body: FutureBuilder(
        future: DataHelper.getProductApi(),
        builder: (BuildContext contex, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            productList = snapshot.data;
            return makeList();
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  makeList() {
    return ListView.builder(
        itemCount: productList.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: ListTile(
              leading: Container(
                width: 36,
                height: 36,
                child: Image.network(productList[index].url),
              ),
              title: Text(productList[index].title),
              subtitle: Text(productList[index].description),
              trailing: IconButton(
                icon: Icon(Icons.add_shopping_cart),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (contex) => AlertDialog(
                      title: const Text('Product successfully added to basket'),
                      actions: <Widget>[
                        new FlatButton(
                          onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text('Ok'))
                      ]
                    )
                  );
                  setState(() {
                    DataHelper.addTOBasket(productList[index]);
                  });
                },
              ),
              onTap: () {
                Navigator.of(context).push(CupertinoPageRoute(
                    builder: (context) => DetailScreen(productList[index])));
              },
            ),
          );
        });
  }
}
