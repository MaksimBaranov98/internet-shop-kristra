import 'package:flutter/material.dart';
import 'package:internet_shop/data/dataHelper.dart';
import 'package:internet_shop/models/ProductModel.dart';

class DetailScreen extends StatefulWidget {
  final ProductModel model;
  DetailScreen(this.model) : super();

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.model.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AspectRatio(
            aspectRatio: 1,
            child: Image.network(widget.model.url, fit: BoxFit.cover),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.model.title,
                    style:
                        TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
                Text(widget.model.description,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.normal)),
                Row(
                  children: [
                    Expanded(
                      child: Text(widget.model.price + " USD",
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.normal)),
                    ),
                    Expanded(
                      child: RaisedButton(
                          onPressed: () {
                            setState(() {
                              DataHelper.addTOBasket(widget.model);
                            });
                          },
                          child: Text('Buy'),
                          padding: EdgeInsets.all(16)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
