class ProductModel {
  final String title;
  final String description;
  final String price;
  final String url;

  ProductModel(this.title, this.description, this.price, this.url);

  static ProductModel fromJson(data) {
    return ProductModel(
        data['title'], data['description'], data['price'], data['url']);
  }
}
