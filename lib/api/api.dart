import 'package:http/http.dart' as http;

class API {
  static Future<http.Response> getProducts() async {
    var data = await http.get(
        "https://6002d5854f17c80017558383.mockapi.io/maksim_baranov_shop/product");

    if (data.statusCode == 200) {
      return data;
    }

    return null;
  }
}
