import 'package:flutter/material.dart';
import 'package:internet_shop/pages/products/Products.dart';
import 'package:internet_shop/pages/basket/Basket.dart';
import 'package:internet_shop/pages/userProfile/userProfile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.dark,
      ),
      home: MyHomePage(title: 'Flutter Demon Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Maksim Baranov Shop"),
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: [ProductsScreen(), BasketScreen(), UserProfile()],
      ),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (selectionIndex) {
            setState(() {
              _currentIndex = selectionIndex;
            });
          },
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.list), label: 'Catalog'),
            BottomNavigationBarItem(icon: Icon(Icons.bathtub_sharp), label: 'Basket'),
            BottomNavigationBarItem(icon: Icon(Icons.people), label: 'My Profile'),
          ]),
    );
  }
}
